
#include "_UskinLibrary.au3"
#include <File.au3>

#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <SliderConstants.au3>
#include <GuiSlider.au3>
#include <Array.au3> 
#include <GuiComboBox.au3>
Opt("GUIOnEventMode", 1) 
$GUI = GUICreate("SKINSELECTOR",300,110,-1,-1,-1,-1)


$button229 = GUICtrlCreateButton("OK",5,66,283,28,-1,-1)
GUICtrlSetOnEvent(-1,"skin")
$label84 = GUICtrlCreateLabel("Select your favourite skin thenpress OK",8,10,283,15,1,-1)
$combo944 = GUICtrlCreateCombo("",9,36,277,21,-1,-1)
$foldercontent = _FileListToArray(@ScriptDir&"\skins\")
$foldercontentstring= _ArrayToString($foldercontent,"|")
GUICtrlSetData(-1,$foldercontentstring)
GUISetState(@SW_SHOW,$GUI)
While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
GUIDelete()

	EndSwitch
WEnd
Func skin()

$file=FileOpen("skins.txt",2)
$idd= _GUICtrlComboBox_GetCurSel($combo944)

FileWrite($file,$foldercontent[$idd])
FileClose($file)
GUIDelete()
Exit
EndFunc
